use std::thread;

use libcwtch;
use libcwtch::structs::*;
use libcwtch::CwtchLib;
use libcwtch::event::Event;

fn main() {
    let bot_home = "example_cwtch_dir";
    match std::fs::remove_dir_all(&bot_home) {
        // this will always error on first run as we haven't created the directory, it's just a function to clear it on successive runs, so this error is to be ignored
        _ => (),
    }
    std::fs::create_dir_all(&bot_home).expect("Error creating bot_home directory");

    let cwtch = libcwtch::new_cwtchlib_go();
    println!("start_cwtch");
    let ret = cwtch.start_cwtch(bot_home, "");
    println!("start_cwtch returned {}", ret);

    let event_loop_handle = thread::spawn(move || {
        loop {
            let event = cwtch.get_appbus_event();
            println!("event: {:?}", event);

            match event {
                Event::CwtchStarted => {
                    println!("event CwtchStarted!");
                    println!("Creating bot");
                    cwtch.create_profile("Echobot", "be gay do crime", true);
                }
                Event::NewPeer { profile_id, tag, created, name, default_picture, picture, online, profile_data } => {
                    println!(
                        "\n***** {} at {} *****\n",
                        name, profile_id.as_str()
                    );

                    // process json for profile, contacts and servers...else {
                    let profile = profile_data;
                    print!("profile: {:?}", profile);
                }
                Event::NewMessageFromPeer { profile_id, conversation_id, contact_id: contact, nick, timestamp_received, message, notification, picture } => {
                    let response = MessageWrapper { o: message.o.into(), d: message.d };
                    cwtch.send_message( &profile_id, conversation_id, &response);
                }
                _ => eprintln!("unhandled event!"),
            };
        }
    });

    event_loop_handle.join().expect("Error running event loop");
}
