# libCwtch-rs

Rust bindings for [libCwtch autobindings](https://git.openprivacy.ca/cwtch.im/autobindings/)

Example echobot in examples/echobot.rs (`cargo run --example echobot` -- assumes tor is on $PATH)

## Building

### Updating libCwtch and bingings.rs with Bindgen 

libCwtch.so version is specified in build.rs. If updating, also download the corresponding libCwtch.h and delete 
the 'preamble from import "C"' section as it imports headers required for the C lib to compile
but that we don't want to create rust bindings for (like importing stdlib.h). `cargo build` automatically calls 
`bindgen` for us and will regenerate `src/cwtchlib_go/bindings.rs` if libCwtch.h has changed. 

While developing you can use the `LCG_DIR` environment variable to specify the directory containing a local `libCwtch.so`
library to override the default one.

This is useful in cases where you are adding or updating APIs prior to a release.