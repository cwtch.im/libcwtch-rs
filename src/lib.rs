#![doc = include_str!( "../README.md" )]
#![doc(html_logo_url = "https://git.openprivacy.ca/cwtch.im/cwtch-ui/media/branch/trunk/cwtch.png")]
#![doc(html_root_url = "https://git.openprivacy.ca/cwtch.im/libcwtch-rs")]
#![deny(missing_docs)]

use crate::event::{ConversationID, Event, FileKey, ProfileIdentity, ServerIdentity};
use crate::structs::{ACL, MessageWrapper, Settings, SharedFile};

mod bindings_go;
mod cwtchlib_go;

/// Basic structs using data from Cwtch and for deserializing JSON and serializing to JSON to communicate with Cwtch
pub mod structs;
/// Additional structs for advnaced event handling and converstion helpers
pub mod event;

/// Error type for Cwtch lib related errors, intended for use with Result
pub type CwtchError = String;

/// Interface to a Cwtch app with API matching libcwtch
pub trait CwtchLib {
    /// Start a cwtch application using app_dir to store all user profile data and looking to tor_path to find tor to run
    fn start_cwtch(&self, app_dir: &str, tor_path: &str) -> i32;

    /// Return 1 if cwtch has been started and 0 if not
    fn started(&self) -> i32;

    /// Like StartCwtch, but StartCwtch has already been called so we don't need to restart Tor etc (probably)
    /// Do need to re-send initial state tho, eg profiles that are already loaded (used by android when ui state is lost)
    fn reconnect_cwtch_foreground(&self);

    /// Save and update the settings based on this settings
    fn update_settings(&self, settings: &Settings);

    /// Activate the networking engine of a profile so it can send and listen to/for messages
    fn activate_peer_engine(&self, profile: &ProfileIdentity);

    /// Deactivate the networking engine of a profile so it cannot send and listen to/for messages
    fn deactivate_peer_engine(&self, profile: &ProfileIdentity);

    /// Pull json of a structs::CwtchEvent off the appbus for responding to
    fn get_appbus_event(&self) -> Event;

    /// configure a peer's connection settings, listen for incoming connections, connect to peers and connect to servers.
    fn configure_connections(&self, profile: &ProfileIdentity, listen: bool, peers: bool, servers: bool);

    /// Create a new profile encrypted with pass
    fn create_profile(&self, nick: &str, pass: &str, autostart: bool);

    /// Load any profiles encrypted by pass
    fn load_profiles(&self, pass: &str);

    /// Cause profile to accept conversation
    fn accept_conversation(&self, profile: &ProfileIdentity, conversation_id: ConversationID);

    /// Cause profile to block conversation
    fn block_conversation(&self, profile: &ProfileIdentity, conversation_id: ConversationID);

    /// Cause profile to unblock conversation
    fn unblock_conversation(&self, profile: &ProfileIdentity, conversation_id: ConversationID);

    /// Attempt to peer with a a new peer
    fn peer_with(&self, profile: &ProfileIdentity, new_peer_address: &str);

    /// manually disconnect from a conversation
    fn disconnect_from_peer(&self, profile: &ProfileIdentity, peer_id: &str);

    /// starts a search process on a profile for pattern. returns a searchID that resulting SearchResult messages will have
    fn search_conversations(&self, profile: &ProfileIdentity, pattern: &str) -> String;
    
    /// Get an ACL for a conversation
    fn get_conversation_access_control_list(&self, profile: &ProfileIdentity, conversation_id: ConversationID) -> Result<ACL, CwtchError>;
    
    /// Update a conversation's ACL
    fn update_conversation_access_control_list(&self, profile: &ProfileIdentity, conversation_id: ConversationID, acl: ACL);

    /// Get a specific message for a conversation by its id
    fn get_message_by_id(&self, profile: &ProfileIdentity, conversation_id: ConversationID, message_id: i32) -> String;

    /// Get a specific message for conversation of profile by hash
    fn get_message_by_content_hash(
        &self,
        profile: &ProfileIdentity,
        conversation_id: ConversationID,
        hash: &str,
    ) -> String;

    /// Bulk get messages starting at message index and of count amoung
    fn get_messages(&self, profile: &ProfileIdentity, conversation_id: ConversationID, message_index: i32, count: u32)  -> String;

    /// Send json of a structs::Message from profile to contact. Returns computed sent message (including index and hash values)
    fn send_message_raw(&self, profile: &ProfileIdentity, conversation_id: ConversationID, msg: &str) -> String;

    /// Send structs::Message from profile to contact. Returns computed sent message (including index and hash values) or Error
    fn send_message(&self, profile: &ProfileIdentity, conversation_id: ConversationID, message: &MessageWrapper) -> Result<String, CwtchError>;

    /// Send profile's contact an invite for/to target. Returns computed sent message (including index and hash values)
    fn send_invite_message(&self, profile: &ProfileIdentity, conversation_id: ConversationID, target_id: i32) -> String;

    /// share a file file_path with a conersation. Returns computed sent message (including index and hash values)
    fn share_file(&self, profile: &ProfileIdentity, conversation_id: ConversationID, file_path: &str) -> String;

    /// get list of SharedFile for a conversation
    fn get_shared_files(&self, profile: &ProfileIdentity, conversaion_id: ConversationID) -> Vec<SharedFile>;

    /// download a file from a conversation to the file_path
    fn download_file_default_limit(
        &self,
        profile: &ProfileIdentity,
        conversation_id: ConversationID,
        file_path: &str,
        manifest_path: &str,
        file_key: &FileKey,
    );

    /// Restart a fileshare (used when restarting app to reoffer a previously created fileshare)
    fn restart_fileshare(&self, profile: &ProfileIdentity, file_key: &FileKey);

    /// Stop offering a fileshare
    fn stop_fileshare(&self, profile: &ProfileIdentity, file_key: &FileKey);

    /// Query the status of a download
    fn check_download_status(&self, profile: &ProfileIdentity, file_key: &FileKey);

    /// Verufy a download is done, and if not, resume it
    fn verify_or_resume_download(&self, profile: &ProfileIdentity, conversation_id: ConversationID, file_key: &FileKey);

    /// Ask the ACN inside the Cwtch app to restart the tor connection
    fn reset_tor(&self);

    /// Cause profile to create a group on server with name
    fn start_group(&self, profile: &ProfileIdentity, name: &str, server: &str);

    /// Queue joining a server in the out going connections queue
    fn queue_join_server(&self, profile: &ProfileIdentity, server: &ServerIdentity);

    /// Disconnect from a specific server
    fn disconnect_from_server(&self, profile: &ProfileIdentity, server: &ServerIdentity);

    /// Publish server status updates for a profile
    fn publish_server_update(&self, profile: &ProfileIdentity);

    /// Get list of known servers for a profile
    fn get_server_info_list(&self, profile: &ProfileIdentity);

    /// Delete server information from a profile, preventing future connections for all groups hosted on it
    fn delete_server_info(&self, profile: &ProfileIdentity, server: &ServerIdentity);

    /// Delete profile with encryption/password check of pass
    fn delete_profile(&self, profile: &ProfileIdentity, pass: &str);

    /// Cause profile to archive conversation with contact
    fn archive_conversation(&self, profile: &ProfileIdentity, conversation_id: ConversationID);

    /// Cause profile to delete contact/group identified by handle
    fn delete_conversation(&self, profile: &ProfileIdentity, conversation_id: ConversationID);

    /// Cuase profile to attempt to import a contact/group/keybundle identified by bundle
    fn import_bundle(&self, profile: &ProfileIdentity, bundle: &str);

    /// Set a profile attribute key to val
    fn set_profile_attribute(&self, profile: &ProfileIdentity, key: &str, val: &str);

    /// Get a profile attribute
    fn get_profile_attribute(&self, profile: &ProfileIdentity, key: &str) -> Result<Option<String>, CwtchError>;

    /// Set a profile's contact's attribute of key to val
    fn set_conversation_attribute(&self, profile: &ProfileIdentity, conversation_id: ConversationID, key: &str, val: &str);

    /// Update an attribute on a message in a conversation
    fn update_message_attribute(
        &self,
        profile: &ProfileIdentity,
        conversation_id: ConversationID,
        channel_id: i32,
        message_id: i32,
        attribute_key: &str,
        attribute_value: &str,
    );

    /// Get an attribute for a conversation
    fn get_conversation_attribute(&self, profile: &ProfileIdentity, conversation_id: ConversationID, key: &str) -> Result<Option<String>, CwtchError>;

    /// Change a profile's password to new_pass if old_pass is correct
    fn change_password(&self, profile: &ProfileIdentity, old_pass: &str, new_pass: &str, new_pass_again: &str);

    /// Export a profile to filename
    fn export_profile(&self, profile: &ProfileIdentity, filename: &str);

    /// Import a profile from a file with supplied password. Json of a profile struct returned on success
    fn import_profile(&self, filename: &str, password: &str) -> String;

    /// Shutdown the cwtch app and associated ACN
    fn shutdown_cwtch(&self);

    /// Server functions require server experiment to be enabled

    /// Load all servers encrypted by password
    fn load_servers(&self, password: &str);

    /// Create a new server, encrypted with password
    fn create_server(&self, password: &str, description: &str, autostart: bool);

    /// Delete the specified server (if password is correct)
    fn delete_server(&self, server: ServerIdentity, current_password: &str);

    /// Launch all loaded servers
    fn launch_servers(&self);

    /// Launch the specified server
    fn launch_server(&self, server: ServerIdentity);

    /// Stop the specified server
    fn stop_server(&self, server: ServerIdentity);

    /// Stop all running servers
    fn stop_servers(&self);

    /// Destroy all servers leaving htem un-re-runnable. call only on shutdown
    fn destroy_servers(&self);

    /// Set the specified server's attribute of key to val
    fn set_server_attribute(&self, server: ServerIdentity, key: &str, val: &str);

    /// Get debug info (mem, goroutine stats) from lcg in json
    fn get_debug_info(&self) -> String;
}

/// Create a new CwtchLib that is backed by bindings to libcwtch-go
pub fn new_cwtchlib_go() -> impl CwtchLib {
    bindings_go::CwtchLibGo {}
}
