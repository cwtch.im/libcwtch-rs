use std::{env, io};
use std::fs;
use std::path::{Path, PathBuf};
use std::process::Command;

use hex_literal::hex;
use sha2::{Digest, Sha512};

fn main() {

    // Do not fetch lib on docs.rs as it cannot, build will fail, docs won't build
    // https://docs.rs/about/builds
    if std::env::var("DOCS_RS").is_err() {
        let out_dir = env::var_os("OUT_DIR").unwrap();
        println!("cargo:rustc-flags=-L {}", out_dir.to_str().unwrap());
        println!("cargo:rustc-link-lib=Cwtch");

        println!("cargo:rerun-if-changed=build.rs");
        println!("cargo:rerun-if-changed=libCwtch.h");


        // The bindgen::Builder is the main entry point
        // to bindgen, and lets you build up options for
        // the resulting bindings.
        let bindings = bindgen::Builder::default()
            // The input header we would like to generate
            // bindings for.
            .header("libCwtch.h")
            // Tell cargo to invalidate the built crate whenever any of the
            // included header files changed.
            .parse_callbacks(Box::new(bindgen::CargoCallbacks))
            // Finish the builder and generate the bindings.
            .generate()
            // Unwrap the Result and panic on failure.
            .expect("Unable to generate bindings");

        // Write the bindings to the $OUT_DIR/bindings.rs file.
        let out_path = PathBuf::from("src/cwtchlib_go");
        bindings
            .write_to_file(out_path.join("bindings.rs"))
            .expect("Couldn't write bindings!");

        let lib_cwtch_path = Path::new(&out_dir).join("libCwtch.so");
        if std::env::var("LCG_DIR").is_err() {
            Command::new("wget")                
                .arg("https://git.openprivacy.ca/cwtch.im/autobindings/releases/download/v0.0.12/libCwtch.x64.so")
                .arg("-O")
                .arg(lib_cwtch_path.clone())
                .output()
                .expect("failed to download libCwtch.x64.so");

            let mut hasher = Sha512::new();
            let mut file = fs::File::open(&lib_cwtch_path).expect("could not open lib to hash");
            io::copy(&mut file, &mut hasher).expect("failed to copy file into hasher");
            let hash_bytes = hasher.finalize();

            assert_eq!(hash_bytes[..], hex!("a3742e0cdedc00eb3673063b100b7596b05bdc9bb68a0fba6fd2423c9a41f1653597c4d44ba3877be08cbb5bba2806435830061ab3517cdd4472a18c7557d9c7")[..]);
        } else {
            let local_lcg = Path::new(std::env::var("LCG_DIR").unwrap().as_str()).join("libCwtch.so");
            fs::copy(local_lcg, lib_cwtch_path).expect("could not find local lcg");
        }
    }
}
